/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.irc.twitchconnection;

import std.algorithm;
import std.container.dlist;
import std.conv : to;
import std.functional;
import std.json;
import std.socket;
import std.stdio;
import std.string;
import std.typecons;

import core.sync.mutex;
import core.thread;

import dialect;

import hotzenbot.irc.ircwritethread;
import hotzenbot.irc.twitchreadthread;
import hotzenbot.irc.bufferedsocket;
import hotzenbot.bot.messagehandler;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;
import hotzenbot.sys.transport;

class TwitchConnection : Transport
{
    this(JSONValue config, BotMessageHandler *handler)
    {
        this.config = config;
        client.nickname = config["twitch"]["account"].str;
        server.address = "irc.twitch.tv";
        this.port = 6667;
        this.message_handler = handler;
        parser = IRCParser(client, server);
    }

    void connect()
    {
        log_info(LoggingScope.Twitch, "Resolving addresses");
        auto addresses = getAddress(this.server.address, this.port);

        if (addresses.length == 0)
        {
            log_fatal(LoggingScope.Twitch, "Could not resolve address.");
            return;
        }

        log_info(LoggingScope.Twitch, "Choosing first address: " ~ addresses[0].to!string);

        socket = new BufferedTcpSocket(addresses[0]);

        if (!socket.isAlive)
        {
            log_error(LoggingScope.Twitch, "Could not connect to TMI.");
            return;
        }

        // Start the write thread
        write_thread = new IRCWriteThread(&socket);
        write_thread.start();
        auto pass_line = cast(const(void)[])("PASS oauth:" ~ this.token ~ "\r\n");
        auto nick_line = cast(const(void)[])("NICK " ~ config["twitch"]["account"].str ~ "\r\n");
        auto tags_line = cast(const(void)[])("CAP REQ :twitch.tv/tags\r\n");

        log_info(LoggingScope.Twitch, "Authenticating");
        socket.send(pass_line);
        socket.send(nick_line);
        socket.send(tags_line);

        read_thread = new TwitchReadThread(&socket, &parser, toDelegate(&on_message_received));
        string answer = read_thread.get_raw_message();

        if (answer.startsWith(":tmi.twitch.tv 001"))
        {
            is_connected = true;
            log_info(LoggingScope.Twitch, "Successfully connected to Twitch");
        }

        read_thread.start();
    }

    void auto_join_channels()
    {
        log_info(LoggingScope.Twitch, "auto-joining registered Twitch channels");
        auto channel_names = message_handler.database.hotzenbot_exec("SELECT name FROM JoinedTwitchChannels;");

        foreach (channel_name; channel_names)
        {
            join_channel(channel_name.peek!string(0));
        }
    }

    void on_message_received(IRCEvent event)
    {
        if (event.type == IRCEvent.Type.RECONNECT)
        {
            log_info(LoggingScope.Twitch, "Received RECONNECT event");
            try
            {
                log_info(LoggingScope.Twitch, "Stopping write thread");
                write_thread.request_stop();
                write_thread.join();
                log_info(LoggingScope.Twitch, "Requesting read thread exit");
                read_thread.request_stop();
                // Do NOT join the thread, this would cause a dead
                // lock since we are in the read thread ourselves here.
                log_info(LoggingScope.Twitch, "Closing socket");
                socket.shutdown(SocketShutdown.BOTH);
                socket.close();
            }
            catch (Exception e)
            {
                log_error(LoggingScope.Twitch, "RECONNECT event error: " ~ e.to!string);
            }

            log_info(LoggingScope.Twitch, "Reconnecting to Twitch");
            connect();
            return;
        }
        else
        {
            message_handler.on_twitch_event(event, this.write_thread, this.client.nickname, this.token, this.client_id);
        }
    }

    string client_id()
    {
        return config["twitch"]["clientId"].str;
    }

    string token()
    {
        return config["twitch"]["token"].str;
    }

    void send_message(string message)
    {
        write_thread.push_message(message);
    }

    void join_channel(string channel)
    {
        send_message("JOIN " ~ channel ~ "\r\n");
    }

    void part_channel(string channel)
    {
        send_message("PART " ~ channel ~ "\r\n");
    }

    void say(string channel, string message)
    {
        send_message("PRIVMSG " ~ channel ~ " :" ~ message ~ "\r\n");
    }

    Result!string set_call_prefix(string channel, char prefix)
    {
        try
        {
            message_handler.database.hotzenbot_exec("UPDATE JoinedTwitchChannels SET channelCommandPrefix=:prefix WHERE name=:name ;",
                                             prefix.to!string, channel);
            return Result!string.ok("Updated call prefix");
        }
        catch (Exception e)
        {
            auto err_msg = "Failed to set command prefix for twitch channel " ~ channel ~ ": " ~ e.to!string;
            log_error(LoggingScope.Twitch, err_msg);
            return Result!string.fail(err_msg);
        }
    }

    void exit()
    {
        read_thread.request_stop();
        write_thread.request_stop();
        read_thread.join();
        write_thread.join();
    }

    JSONValue config;
    BufferedTcpSocket socket;
    IRCClient client;
    IRCServer server;
    IRCParser parser;
    bool is_connected = false;
    IRCWriteThread write_thread;
    TwitchReadThread read_thread;
    BotMessageHandler* message_handler;
    ushort port;
}
