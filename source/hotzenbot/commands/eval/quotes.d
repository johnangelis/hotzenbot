module hotzenbot.commands.eval.quotes;

import hotzenbot.commands.context;
import hotzenbot.commands.irccontext;
import hotzenbot.commands.twitchcontext;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

import d2sqlite3;

import std.algorithm;
import std.array;
import std.conv : to;
import std.datetime;
import std.random;

struct QuoteEntry
{
    string quote;
    string date;

    string pretty()
    {
        return "\"" ~ quote ~ "\" -- " ~ date;
    }
}

EvalResult _get_quote(Database* db, string network, string channel)
{
    try
    {
        auto rows = db.hotzenbot_exec("SELECT quoteMessage, quoteDate FROM Quotes WHERE quoteNetwork = :net AND quoteChannel = :channel;",
                                      network, channel)
            .map!(x => QuoteEntry(x["quoteMessage"].as!string, x["quoteDate"].as!string))
            .array;

        if (rows.empty)
        {
            return EvalResult.fail("No quotes added to the database in this channel.");
        }

        auto quote = rows.choice();
        return EvalResult.ok(quote.pretty);
    }
    catch (Exception e)
    {
        log_error(LoggingScope.System, "Unable to fetch from the quote database: " ~ e.to!string);
        return EvalResult.fail("Unable to fetch from the database.");
    }
}

EvalResult get_twitch_quote(TwitchContext ctx)
{
    return _get_quote(ctx.database, "twitch", ctx.channel);
}

EvalResult get_irc_quote(IRCContext ctx)
{
    return _get_quote(ctx.database, ctx.network, ctx.channel);
}

EvalResult get_quote(Context ctx)
{
    TwitchContext tctx = cast(TwitchContext)ctx;
    if (tctx !is null)
    {
        return get_twitch_quote(tctx);
    }

    auto ictx = cast(IRCContext)ctx;
    if (ictx !is null)
    {
        return get_irc_quote(ictx);
    }

    return EvalResult.fail("Unable to fetch quotes in a non-IRC or non-Twitch context.");
}

EvalResult _add_quote(Database* db, string network, string channel, string quote_text)
{
    try
    {
        auto today = Clock.currTime();
        db.hotzenbot_exec(
            "INSERT INTO Quotes (quoteNetwork, quoteChannel, quoteDate, quoteMessage) VALUES (:net, :chan, :dat, :msg);",
            network, channel, Date(today.year, today.month, today.day).toSimpleString, quote_text);
        return EvalResult.ok("Added quote");
    }
    catch (Exception e)
    {
        log_error(LoggingScope.System, "Unable to add quote to the quote database: " ~ e.to!string);
        return EvalResult.fail("Unable to add quote to the database.");
    }
}

EvalResult add_twitch_quote(TwitchContext ctx, string quote_text)
{
    return _add_quote(ctx.database, "twitch", ctx.channel, quote_text);
}

EvalResult add_irc_quote(IRCContext ctx, string quote_text)
{
    return _add_quote(ctx.database, ctx.network, ctx.channel, quote_text);
}

EvalResult add_quote(Context ctx, string quote_text)
{
    TwitchContext tctx = cast(TwitchContext)ctx;
    if (tctx !is null)
    {
        return add_twitch_quote(tctx, quote_text);
    }

    auto ictx = cast(IRCContext)ctx;
    if (ictx !is null)
    {
        return add_irc_quote(ictx, quote_text);
    }

    return EvalResult.fail("Unable to add quotes in a non-IRC or non-Twitch context.");
}
