/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.eval.uptime;

import hotzenbot.commands.context;
import hotzenbot.commands.twitchcontext;
import hotzenbot.sys.result;
import hotzenbot.sys.twitchapi;

import std.conv;
import std.datetime;
import std.json;
import std.range;

EvalResult uptime(Context ctx, string channel)
{
    TwitchContext tctx = cast(TwitchContext)ctx;
    if (tctx is null)
    {
        return EvalResult.fail("%uptime() only works on Twitch");
    }

    string url = "https://api.twitch.tv/helix/streams?user_login=";
    if (channel.empty)
        url ~= tctx.channel[1..$];
    else
        url ~= channel;

    auto stream_request_result = perform_twitch_api_request(tctx.credentials,  url);
    if (stream_request_result.is_error)
    {
        return stream_request_result;
    }

    auto parsed_body = parseJSON(stream_request_result.result);
    if ("data" !in parsed_body)
    {
        return EvalResult.fail("Invalid Twitch API response");
    }

    if (parsed_body["data"].array.empty)
    {
        return EvalResult.ok("Channel appears to be offline");
    }

    auto started = SysTime.fromISOExtString(
        parsed_body["data"].array.front["started_at"].str);

    Duration uptime_of_stream = Clock.currTime.toUTC - started;
    long hours = uptime_of_stream.total!"hours"();
    long minutes = uptime_of_stream.total!"minutes" - 60 * hours;

    return EvalResult.ok(hours.to!string ~ " hrs and " ~ minutes.to!string ~ " minutes");
}
