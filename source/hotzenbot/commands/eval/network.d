/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.eval.network;

import hotzenbot.sys.logging;
import hotzenbot.sys.result;

import std.string;

EvalResult curl(string url)
{
    import std.net.curl;

    try
    {
        return EvalResult.ok(cast(string)(url.get.dup));
    }
    catch (Exception e)
    {
        log_error(LoggingScope.System, "Unable to curl url `" ~ url ~ "`.");
        return EvalResult.fail("Evaluation error. The bot maintainer should look at the logs.");
    }
}

EvalResult urlencode(string message)
{
    import std.uri;

    return EvalResult.ok(message.encode);
}
