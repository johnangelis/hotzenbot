/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.eval.polls;

import hotzenbot.bot.pollsthread;
import hotzenbot.commands.context;
import hotzenbot.commands.eval.timer;
import hotzenbot.commands.expression;
import hotzenbot.commands.irccontext;
import hotzenbot.commands.twitchcontext;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

import std.algorithm;
import std.array;
import std.conv;
import std.exception;
import std.range;

EvalResult poll_vote(Context ctx, string arg)
{
    int option = arg.parse!int.ifThrown(-1);

    if (option < 1)
    {
        return EvalResult.fail("Please revise your basic knowledge on algebra");
    }

    TwitchContext tctx = cast(TwitchContext)(ctx);
    if (tctx !is null)
    {
        auto v = Vote("twitch", tctx.channel, tctx.sender, cast(uint)(option));
        vote_in_poll(cast(shared)v);
        return EvalResult.ok("Voted");
    }

    IRCContext ictx = cast(IRCContext)(ctx);
    if (ictx !is null)
    {
        auto v = Vote(ictx.network, ictx.channel, ictx.sender, cast(uint)(option));
        vote_in_poll(cast(shared)v);
        return EvalResult.ok("Voted");
    }

    return EvalResult.fail("Can only vote on Twitch and IRC");
}

EvalResult poll_start(Context ctx, Expression[] args)
{
    if (args.length != 3)
    {
        return EvalResult.fail("Need 2 arguments for %poll_start(duration, title, options)");
    }

    EvalResult maybe_duration = args[0].evaluate(ctx);
    if (maybe_duration.is_error)
    {
        return maybe_duration;
    }

    auto maybe_poll_end_time = maybe_duration.result.parse_time();
    if (maybe_poll_end_time.is_error)
    {
        return EvalResult.fail(maybe_poll_end_time.err_msg);
    }

    auto maybe_title = args[1].evaluate(ctx);
    if (maybe_title.is_error)
    {
        return maybe_title;
    }

    auto maybe_options = args[2].evaluate(ctx);
    if (maybe_options.is_error)
    {
        return maybe_options;
    }

    StartPollRequest request;

    TwitchContext tctx = cast(TwitchContext)(ctx);
    if (tctx !is null)
    {
        request.network = "twitch";
        request.channel = tctx.channel;
    }

    IRCContext ictx = cast(IRCContext)(ctx);
    if (ictx !is null)
    {
        request.network = ictx.network;
        request.channel = ictx.channel;
    }

    if (ictx is null && tctx is null)
    {
        return EvalResult.fail("Polls only work on Twitch and IRC");
    }

    request.poll_end_time = maybe_poll_end_time.result;
    request.options = maybe_options.result;
    request.title = maybe_title.result;

    start_poll(cast(shared)(request));

    return EvalResult.ok("Poll has been started. Use the vote command with one of the following options: "
                         ~ maybe_options.result
                         .split(',')
                         .enumerate
                         .map!(option => (option[0] + 1).to!string ~ " for " ~ option[1])
                         .join(", "));
}
