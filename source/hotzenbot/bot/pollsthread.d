/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.bot.pollsthread;

import std.algorithm;
import std.array;
import std.concurrency;
import std.conv : to;
import std.datetime;
import std.parallelism;

import core.thread;

import hotzenbot.sys.logging;
import hotzenbot.sys.transport;

struct StartPollRequest
{
    string network;
    string channel;
    SysTime poll_end_time;
    string title;
    string options;

    Poll to_poll()
    {
        Poll p;
        p.network = network;
        p.channel = channel;
        p.poll_end_time = poll_end_time;
        p.options = options.split(',').array;
        p.title = title;
        return p;
    }
}

struct Poll
{
    string network;
    string channel;
    string[] options;
    string title;
    Vote[] votes;
    SysTime poll_end_time;

    bool has_user_already_voted(string user_name)
    {
        return votes.any!(x => x.user_name == user_name);
    }

    Duration remaining_time()
    {
        return poll_end_time - Clock.currTime();
    }
}

struct Vote
{
    string network;
    string channel;
    string user_name;
    uint option;
    uint votes = 1;
}

class PollsThread : Thread
{
    this()
    {
        super(&polls_worker);
    }

    void request_exit()
    {
        this.should_quit = true;
    }

    void add_transport_layer(string name, Transport t)
    {
        transports[name] = t;
        log_info(LoggingScope.Polls, "Registered transport " ~ name);
    }

    void polls_worker()
    {
        register("polls", thisTid);

        while (!should_quit)
        {
            receiveTimeout(dur!"seconds"(1),
                           (shared(StartPollRequest) p)
                           {
                               log_info(LoggingScope.Polls, "Poll has been started");
                               polls ~= (cast(StartPollRequest)(p)).to_poll();
                           },
                           (shared(Vote) v)
                           {
                               log_info(LoggingScope.Polls, "Received a vote");
                               for (uint i = 0; i < polls.length; ++i)
                               {
                                   if (polls[i].channel == v.channel && polls[i].network == v.network)
                                   {
                                       // Invalid vote number
                                       if (v.option == 0 || v.option > polls[i].options.length) return;

                                       // has user already voted? if not, count the vote
                                       if (!polls[i].has_user_already_voted(v.user_name))
                                       {
                                           polls[i].votes ~= v;
                                       }

                                       return;
                                   }
                               }

                               log_info(LoggingScope.Polls, "Vote for a non-existing poll");
                           }
                );
            handle_polls();
        }
    }

    void handle_polls()
    {
        foreach_reverse (poll_idx, poll; polls)
        {
            auto time_to_go = poll.remaining_time();
            if (time_to_go.total!"seconds"() <= 0)
            {
                // Poll is over
                string message = "The poll is over. Title: " ~ poll.title ~ ". Results: ";

                for (uint i = 0; i < poll.options.length; ++i)
                {
                    auto total_votes = poll.votes
                        .filter!(vote => vote.option - 1 == i)
                        .map!(vote => vote.votes)
                        .sum();
                    message ~= poll.options[i]
                        ~ ": " ~ (total_votes == 1 ? "1 vote. " : total_votes.to!string ~ " votes. ");
                }

                transports[poll.network].say(poll.channel, message);
                log_info(LoggingScope.Polls, "Poll for channel " ~ poll.channel ~ " finished.");
                polls = polls.remove(poll_idx);
            }
            else if (time_to_go.total!"seconds"() % 30 == 0)
            {
                // Announce that the poll is still going
                transports[poll.network].say(poll.channel, "The poll is still going. The question was: " ~ poll.title);
            }
        }
    }

    shared bool should_quit = false;
    Poll[] polls;
    Transport[string] transports;
}

void start_poll(shared StartPollRequest p)
{
    try
    {
        auto tid = locate("polls");
        tid.send(p);
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Polls, "Unable to submit poll request: " ~ e.to!string);
    }
}

void vote_in_poll(shared Vote v)
{
    auto tid = locate("polls");
    tid.send(v);
}
