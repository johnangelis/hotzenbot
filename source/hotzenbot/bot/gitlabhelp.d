/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.bot.gitlabhelp;

import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

import d2sqlite3;

import std.algorithm;
import std.concurrency;
import std.conv;
import std.json;
import std.net.curl;
import std.range;

import core.thread;

enum help_file_path = "help.org";

long create_gitlab_snippet(string access_token, string snippet_title)
{
    auto client = HTTP();
    client.addRequestHeader("PRIVATE-TOKEN", access_token);
    client.addRequestHeader("Content-Type", "application/json");

    JSONValue post_body = ["title": snippet_title, "description": "Hotzenbot Help Page", "visibility": "public"];
    post_body.object["files"] = JSONValue(
        [ JSONValue(["file_path": help_file_path, "content": "* Hotzenbot Help"])
            ]
        );

    auto raw_reply = post("https://gitlab.com/api/v4/snippets", post_body.toString, client);
    auto reply = parseJSON(raw_reply);
    import std.stdio;
    writeln(reply);
    return reply["id"].integer;
}

Result!string update_gitlab_help_snippet(Database* db,
                                         string access_token,
                                         string snippet_title)
{
    try
    {
        auto client = HTTP();
        client.addRequestHeader("PRIVATE-TOKEN", access_token);
        auto raw_snippet_list = cast(string)(get("https://gitlab.com/api/v4/snippets", client));

        long snippet_id = -1;

        // Find snippet id. If snippet doesn't exist, create it.
        auto snippet_list = parseJSON(raw_snippet_list).array;
        if (!snippet_list.any!(x => x["title"].str == snippet_title))
        {
            snippet_id = create_gitlab_snippet(access_token, snippet_title);
        }
        else
        {
            snippet_id = snippet_list.find!((a, b) => a["title"].str == b)(snippet_title).front["id"].integer;
        }

        string table = `* Hotzenbot Help

|Command Names|Definition|
|-
`;

        // Fetch all the command names and codes and generate a row with each command
        auto commands = db.hotzenbot_exec( "SELECT GROUP_CONCAT(name, \", \") names, code"
            ~ " FROM Command JOIN CommandName"
            ~ " ON Command.id=CommandName.commandId"
            ~ " WHERE name IS NOT NULL AND code IS NOT NULL"
            ~ " GROUP BY commandId"
            ~ " ORDER BY CommandName.name;");

        foreach (command; commands)
        {
            table ~= "|" ~ command["names"].as!string ~ "|" ~ command["code"].as!string ~ "|\n";
        }

        // Prepare the PUT request data
        JSONValue put_data = [
            "files": JSONValue(
                [
                    [ "action" : "update",
                      "content": table,
                      "file_path": help_file_path
                    ]
                ])
            ];

        client = HTTP();
        client.addRequestHeader("PRIVATE-TOKEN", access_token);
        client.addRequestHeader("Content-Type", "application/json");
        client.addRequestHeader("User-Agent", "KEKW");
        put("https://gitlab.com/api/v4/snippets/" ~ snippet_id.to!string, put_data.toString(), client);
        return Result!string.ok("Updated the help page");
    }
    catch (Exception e)
    {
        log_error(LoggingScope.System, "Unable to update GitLab help snippet: " ~ e.to!string);
        return Result!string.fail("Unable to update GitLab help snippet");
    }
}

class GitLabThread : Thread
{
    this(Database* db, string gl_access_token, string help_page)
    {
        this.db = db;
        this.gl_access_token = gl_access_token.dup;
        this.gl_help_page = help_page.dup;
        super(&gl_worker);
    }

    void request_exit()
    {
        should_exit = true;
    }

    void gl_worker()
    {
        register("gitlab", thisTid);

        while (!should_exit)
        {

            receive(
                (bool _) {
                    log_info(LoggingScope.System, "Updating Gitlab Help Page");
                    if (update_gitlab_help_snippet(db, gl_access_token, gl_help_page).is_error)
                    {
                        log_error(LoggingScope.System, "Failed to update Gitlab Help Page");
                    }
                    else
                    {
                        log_info(LoggingScope.System, "Updated Gitlab Help Page");
                    }
                });
        }

        log_warn(LoggingScope.System, "GitLab worker thread exited");
        unregister("gitlab");
    }

    Database* db;
    string gl_access_token;
    string gl_help_page;
    shared bool should_exit = false;
}

void update_gitlab_help_page()
{
    auto tid = locate("gitlab");
    log_info(LoggingScope.System, "Trying to update Gitlab Help Page");

    if (tid != Tid.init)
    {
        send(tid, true);
    }
    else
    {
        log_error(LoggingScope.System, "Unable to locate Gitlab thread.");
    }
}
